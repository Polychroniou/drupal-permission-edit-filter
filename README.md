# Edit permission filter for Workbench access module

The module adds a view filter that, if added in a view,  shows only nodes that the user is allowed to update/edit


## Installation

* Install the module as usual. 

## Usage

The module exposes a filter called 

"User is allowed to edit the node"

Steps:

1. Go to your view that lists the nodes i.e. the default "Content" View and on "Filter criteria" add this filter
"User is allowed to edit the node". Save the view
![view_filter](http://www.polychroniou.gr/images/Screen%20Shot%202017-12-04%20at%2000.03.21.png)
2. Create a vocabulary called "channel" and add it (as a relation) to the content type you want to apply workbench access restrictions. 
3. On each node give the Channel that applies to the user section you want to give access
4. Assign sections to editors


##Example

Added filter to the a view called "Articles Admin"


The result for users assigned to channel called politics is:
![view_filter](http://www.polychroniou.gr/images/Screen%20Shot%202017-12-07%20at%2023.20.00.png)

The result for users assigned to channel called sports is:
![view_filter](http://www.polychroniou.gr/images/Screen%20Shot%202017-12-07%20at%2023.30.58.png)
