<?php
/**
 * Created by PhpStorm.
 * User: mythodea
 * Date: 03/12/17
 * Time: 23:17
 */

namespace Drupal\permission_edit_filter\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter by node_access records.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("edit_filter")
 */
class EditPermissionFilter extends FilterPluginBase
{

  public function adminSummary()
  {
  }

  protected function operatorForm(&$form, FormStateInterface $form_state)
  {
  }

  public function canExpose()
  {
    return FALSE;
  }


  public function query()
  {
    $table = $this->ensureMyTable();
    $this->query->addWhereExpression($this->options['group'], "$table.status = 1 OR $table.status = 0");
  }
}